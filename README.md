Polybar


polybar is a fast and easy-to-use tool for creating status bars. It aims to be easily customizable, utilising many modules which enable a wide range of (editable) functionality, such as displaying workspaces, the date, or system volume. Polybar is especially useful for window managers that have a limited or non-existent status bar, such as awesome or i3. Polybar can also be used with desktop environments like Plasma.


Installation

Install the polybarAUR package. The development version is polybar-gitAUR.


If using bspwm, add the following to bspwmrc:

$HOME/.config/polybar/launch.sh

